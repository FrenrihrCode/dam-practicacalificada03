import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/avatar_page.dart';
import 'package:componentes/src/pages/card_page.dart';
import 'package:componentes/src/pages/container_page.dart';
import 'package:componentes/src/pages/detail_page.dart';
import 'package:componentes/src/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        'alert': (context) => AlertPage(),
        'avatar': (context) => AvatarPage(),
        'card': (context) => CardPage(),
        'container': (context) => ContainerPage(),
        'detail': (context) => DetailPage(),
      }
    );
  }
}