import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MaterialApp',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.cyan[900],
        accentColor: Colors.cyan[600],
        // Define the default font family.
        fontFamily: 'Georgia',
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.cyan[700],     //  <-- dark color
          textTheme: ButtonTextTheme.primary, //  <-- this auto selects the right color
        )
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: false,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Vista alertas', style: TextStyle(fontSize: 25)),
              RaisedButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                          title: Text('Una alerta'),
                          content: Text('Contenido de la alerta'),
                      )
                  );
                },
                child: Text("Mostrar alerta"),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );
  }
}