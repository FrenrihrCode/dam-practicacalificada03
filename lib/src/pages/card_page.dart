import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MaterialApp',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Vista Card', style: TextStyle(fontSize: 25)),
              Card(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  color: Colors.blue[200],
                  child: Column(
                    children: <Widget>[
                      Text('Hello World'),
                      Text('How are you?')
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          onPressed: ()=>{
            Navigator.pop(context)
        }),
      ),
    );
  }
}