import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert': Icons.add_alert,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.folder_open,
  'contenedor': Icons.content_paste,
  'detalles': Icons.device_unknown,
  'options': Icons.settings_applications,
  'about': Icons.info
};

Icon getIcon(String nombreIcono){
  return Icon(_icons[nombreIcono]);
}